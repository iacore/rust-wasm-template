# Usage

```
Usage: terser [options] [files...]

Options:
  -V, --version                            output the version number
  -p, --parse <options>                    Specify parser options.
  -c, --compress [options]                 Enable compressor/specify compressor options.
  -m, --mangle [options]                   Mangle names/specify mangler options.
  --mangle-props [options]                 Mangle properties/specify mangler options.
  -f, --format [options]                   Format options.
  -b, --beautify [options]                 Alias for --format.
  -o, --output <file>                      Output file (default STDOUT).
  --comments [filter]                      Preserve copyright comments in the output.
  --config-file <file>                     Read minify() options from JSON file.
  -d, --define <expr>[=value]              Global definitions.
  --ecma <version>                         Specify ECMAScript release: 5, 2015, 2016 or 2017...
  -e, --enclose [arg[,...][:value[,...]]]  Embed output in a big function with configurable arguments and values.
  --ie8                                    Support non-standard Internet Explorer 8.
  --keep-classnames                        Do not mangle/drop class names.
  --keep-fnames                            Do not mangle/drop function names. Useful for code relying on Function.prototype.name.
  --module                                 Input is an ES6 module
  --name-cache <file>                      File to hold mangled name mappings.
  --rename                                 Force symbol expansion.
  --no-rename                              Disable symbol expansion.
  --safari10                               Support non-standard Safari 10.
  --source-map [options]                   Enable source map/specify source map options.
  --timings                                Display operations run time on STDERR.
  --toplevel                               Compress and/or mangle variables in toplevel scope.
  --wrap <name>                            Embed everything as a function with “exports” corresponding to “name” globally.
  -h, --help                               output usage information
```

## Compress

```
Supported options:
  arguments             false
  arrows                true
  booleans              true
  booleans_as_integers  false
  collapse_vars         true
  comparisons           true
  computed_props        true
  conditionals          true
  dead_code             true
  defaults              true
  directives            true
  drop_console          false
  drop_debugger         true
  ecma                  5
  evaluate              true
  expression            false
  global_defs           false
  hoist_funs            false
  hoist_props           true
  hoist_vars            false
  ie8                   false
  if_return             true
  inline                true
  join_vars             true
  keep_classnames       false
  keep_fargs            true
  keep_fnames           false
  keep_infinity         false
  loops                 true
  module                false
  negate_iife           true
  passes                1
  properties            true
  pure_getters          "strict"
  pure_funcs            null
  reduce_funcs          true
  reduce_vars           true
  sequences             true
  side_effects          true
  switches              true
  top_retain            null
  toplevel              false
  typeofs               true
  unsafe                false
  unsafe_arrows         false
  unsafe_comps          false
  unsafe_Function       false
  unsafe_math           false
  unsafe_symbols        false
  unsafe_methods        false
  unsafe_proto          false
  unsafe_regexp         false
  unsafe_undefined      false
  unused                true
  warnings              false
```


## Mangle

```
Supported options:
  cache            null
  eval             false
  ie8              false
  keep_classnames  false
  keep_fnames      false
  module           false
  nth_identifier   {}
  properties       false
  reserved         []
  safari10         false
  toplevel         false
```
