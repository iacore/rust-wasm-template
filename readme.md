# Template of wasm-bindgen & trunk

This template draws a smiley with canvas-Context2D. wasm: 19K js: 5K.

this image below is transparent, open in new tab to see it (if you use dark mode)
![screenshot](help/canvas.png)

## Usage

```
pnpm install -g terser # js minifier
cargo install twiggy # wasm size profiler

trunk build --release # build
```
